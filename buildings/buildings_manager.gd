extends Node2D

var building_active = false
var current_building: Building

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _process(delta):
	if building_active:
		Globals.map_manager.start_highlighting(current_building.size)
		if Input.is_action_just_pressed("place"):
			print("Placed building")
			if Globals.map_manager.place_building(current_building):
				building_active = false		

func start_build_process(building: Building):
	print("Started build Process")
	# Needed so is_action_just_pressed in _process doesnt instantly trigger
	await get_tree().create_timer(0.1).timeout
	building_active = true
	current_building = building

func got_clicked():
	pass
