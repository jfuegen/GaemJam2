extends Node2D

var health = 100
var level = 1
var production = 30

func _on_area_2d_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.pressed:
		Globals.building_manager.got_clicked


func _on_get_ressource_timeout():
	Globals.game.set_ressource("energy", production*level)

func start_timer():
	$GetRessource.start()
