class_name Building
extends Resource

enum Category {production, defense, utility, special}

@export var name: String
@export var category: Category
@export var scene: PackedScene
@export var texture: Texture2D
@export var size: Vector2
