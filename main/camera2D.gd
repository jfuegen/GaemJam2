extends Camera2D

var speed = 10
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var direction = Input.get_vector("camera_left","camera_right","camera_up","camera_down")
	position += direction * (speed / zoom.x)
	if Input.is_action_just_pressed("zoom_in") and zoom <= Vector2(2,2):
		zoom += Vector2(0.1,0.1)
		print(zoom)
	if Input.is_action_just_pressed("zoom_out") and zoom >= Vector2(1,1):
		zoom -= Vector2(0.1,0.1)
		print(zoom)
