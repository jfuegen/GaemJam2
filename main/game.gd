extends Node

@onready var camera = $Camera2D
@onready var buildings_manager = preload("res://buildings/buidings_manager.tscn").instantiate()
@onready var map_manager = preload("res://map/map_manager.tscn").instantiate()
@onready var ui_manager = preload("res://ui/ui_manager.tscn").instantiate()

var ressources = {
	"energy":0,
	"soil":0,
	"metal":0
}

func _ready():
	Globals.game = self
	Globals.buildings_manager = buildings_manager
	Globals.map_manager = map_manager
	Globals.ui_manager = ui_manager
	_build_scene()

func _process(delta):
	if Input.is_action_just_pressed("temp"):
		print(Globals.map.get_local_mouse_position())

func _build_scene():
	add_child(map_manager)
	add_child(buildings_manager)
	$CanvasLayer.add_child(ui_manager)
	ui_manager.set_game_ui_visible()

func set_ressource(res: String, value: int):
	ressources[res] += value
	ui_manager.set_ressources(ressources)

