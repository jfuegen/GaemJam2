extends Node2D

@export var noise_height_text: NoiseTexture2D
@onready var map = $TileMap as TileMap
var height:int = 300
var width:int = 300
var noise: Noise
var gradient_points: Array
var highlight_area = false
var highlighted_area_size: Vector2
var area_blocked = false

# Called when the node enters the scene tree for the first time.
func _ready():
	noise = noise_height_text.noise
	for point in range(noise_height_text.color_ramp.get_point_count()):
		gradient_points.append(noise_height_text.color_ramp.get_offset(point))
	_generate_world()
	
func _process(delta):
	if highlight_area:
		_draw_highlights()
	
func _generate_world():
	for x in range(width):
		for y in range(height):
			var v: float = noise.get_noise_2d(x,y)
			v = (v + 1) / 2
			if v >= gradient_points[0] && v < gradient_points[1]:
				map.set_cell(1, Vector2(x,y),0,Vector2(0,2))
				map.set_cell(0, Vector2(x,y), 0, Vector2(1,7))
			elif v >= gradient_points[1] && v < gradient_points[2]:
				map.set_cell(1, Vector2(x,y),0,Vector2(0,0))
			elif v >= gradient_points[2] && v < gradient_points[3]:
				map.set_cell(1, Vector2(x,y),0,Vector2(0,0))
				map.set_cell(2, Vector2(x,y),1,Vector2(0,1))
			elif v >= gradient_points[3] && v < gradient_points[4]:
				map.set_cell(2, Vector2(x,y),1,Vector2(0,0))
			elif v >= gradient_points[4] && v <= gradient_points[5]:
				map.set_cell(2, Vector2(x,y),1,Vector2(1,0))

func place_building(building: Building) -> bool:
	if !area_blocked:
		var child = building.scene.instantiate()
		child.position = map.map_to_local(map.local_to_map(get_local_mouse_position()))
		add_child(child)
		_mark_as_blocked(_get_area_as_cells(building.size, map.local_to_map(get_local_mouse_position())))
		if child.has_method("start_timer"):
			child.start_timer()
		_stop_highlighting()
		
		return true
	else:
		return false

func start_highlighting(size: Vector2):
	highlighted_area_size = size
	highlight_area = true
	
func _draw_highlights():
	map.clear_layer(3)
	var cells = _get_area_as_cells(highlighted_area_size, map.local_to_map(get_local_mouse_position()))
	var area_just_blocked = false
	for cell in cells:
		if map.get_cell_tile_data(2,cell) or map.get_cell_tile_data(0,cell):
			map.set_cell(3,cell,0,Vector2(0,5))	
			area_just_blocked = true
		else:
			map.set_cell(3,cell,0,Vector2(1,5))
	area_blocked = area_just_blocked

func _get_area_as_cells(size: Vector2, starting_cell: Vector2) -> Array:
	var cells = []
	cells.append(starting_cell)
	for x in range(highlighted_area_size.x):
		for y in range(highlighted_area_size.y):
			cells.append(Vector2(starting_cell.x + x, starting_cell.y - y))
	return cells

func _stop_highlighting():
	map.clear_layer(3)
	highlight_area = false
	highlighted_area_size = Vector2(0,0)

func _mark_as_blocked(area: Array):
	for cell in area:
		map.set_cell(0, cell, 0, Vector2(1,7))
