extends Control

@export var buildings: Array[Building]

func _ready():
	_get_all_buildings()
	_build_menu()

# Used to build an array of all Building Ressources created in the data/buildings path
func _get_all_buildings():
	var dir = DirAccess.open("res://data/ressources/buildings/")
	dir.list_dir_begin()
	var file_name = dir.get_next()
	while file_name != "":
		buildings.append(load("res://data/ressources/buildings/" + file_name))
		file_name = dir.get_next()
	dir.list_dir_end()

# Dynamically builds the build menu with all buildings, connecting the button_down signal to the
# _on_button_pressed method and delivering the corresponding Building Ressource
func _build_menu():
	for building in buildings:
		var texture_button = TextureButton.new()
		texture_button.texture_normal = building.texture
		texture_button.SIZE_FILL
		texture_button.button_down.connect(_on_button_pressed.bind(building))
		match building.category:
			building.Category.production:
				$"TabContainer/Production Buildings".add_child(texture_button)
			building.Category.utility:
				$"TabContainer/Utility Buildings".add_child(texture_button)
			building.Category.defense:
				$"TabContainer/Defense Buildings".add_child(texture_button)
			building.Category.special:
				$"TabContainer/Special Buildings".add_child(texture_button)

func _on_button_pressed(building: Building):
	print(building.name)
	Globals.buildings_manager.start_build_process(building)
