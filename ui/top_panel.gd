extends Control

@onready var energy_value = $HBoxContainer/EnergyContainer/EnergyValue
@onready var soil_value = $HBoxContainer/SoilContainer/SoilValue
@onready var metal_value = $HBoxContainer/MetalContainer/MetalValue

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_energy(value: int):
	energy_value.text = str(value)
func get_energy() -> int:
	return energy_value.text as int
func set_soil(value: int):
	soil_value.text = str(value)
func get_soil() -> int:
	return soil_value.text as int
func set_metal(value: int):
	metal_value.text = str(value)
func get_metal() -> int:
	return metal_value.text as int


