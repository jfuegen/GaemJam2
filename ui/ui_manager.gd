extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	for child in get_children():
		child.visible = false

func set_game_ui_visible():
	$BuildMenu.visible = true
	$TopPanel.visible = true

func set_ressources(ressources: Dictionary):
	$TopPanel.set_energy(ressources["energy"])
	$TopPanel.set_soil(ressources["soil"])
	$TopPanel.set_metal(ressources["metal"])
